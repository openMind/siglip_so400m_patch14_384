import argparse

import torch
import requests
from PIL import Image
from openmind import pipeline, is_torch_npu_available
from openmind_hub import snapshot_download


def parse_args():
    parser = argparse.ArgumentParser(description="Eval the LLM model")
    parser.add_argument(
        "--model_name_or_path",
        type=str,
        help="Path to model",
        default=None,
    )

    args = parser.parse_args()

    return args


def main():
    args = parse_args()
    if args.model_name_or_path:
        model_path = args.model_name_or_path
    else:
        model_path = snapshot_download("PyTorch-NPU/siglip_so400m_patch14_384", revision="main",
                                       resume_download=True, ignore_patterns=["*.h5", "*.ot", "*.msgpack"])
    if is_torch_npu_available():
        device = "npu:0"
    elif torch.cuda.is_available():
        device = "cuda:0"
    else:
        device = "cpu"

    # load pipe
    image_classifier = pipeline(task="zero-shot-image-classification", model=model_path, device=device)

    # load image
    url = 'http://images.cocodataset.org/val2017/000000039769.jpg'
    image = Image.open(requests.get(url, stream=True).raw)

    # inference
    outputs = image_classifier(image, candidate_labels=["2 cats", "a plane", "a remote"])
    outputs = [{"score": round(output["score"], 4), "label": output["label"] } for output in outputs]
    print(outputs)


if __name__ == "__main__":
    main()
 